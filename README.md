***Mapa Sitio web Buscador de empleo UAESPE.***

**IMPORTANTE. RESTRICCIONES DE USO DE ESTE APLICATIVO**


Este aplicativo utiliza las librerías de Javascript Amcharts cuyo uso es gratuito siempre y cuando se mantenga debajo del mapa el enunciado y link: **"js map by amcharts"**  que hace referencia a la autoría de las librerías utilizadas. Para ampliar esta información acerca del uso gratuito consulte el siguiente link:

http://www.amcharts.com/download/


**INSTRUCCIONES PARA SU IMPLEMENTACIÓN** 

IMPORTANTE. Los archivos CSV y SQL necesarios para este proceso se encuentran en la carpeta de setup files.

1. Crear la base de datos **servicio_empleo_mapa**. Correr Script para creación de la base de datos. servicio_empleo_mapa.sql
2. Descargar los archivos de git@github.com:serviciodeempleo/mapa.git (Es necesario autorizar antes al usuario en Github)
3. Crear usuario para la base de datos MySQL. ( mapa_empleo_user / XXXXXXXX ). Es necesario asignarle permisos para hacer inserts, select, update y delete.
4. Ingresar a la direccion donde se coloque el aplicativo y agregarle /import (Ej. www.aplicativo.com/import) para cargar el listado de ciudades y oficinas. Este proceso borra el listado de ciudades y puntos de atención y carga ciudades y puntos del archivo mapa.csv ( que se debe colocar en la carpeta import_files).


**Si se va a utilizar como aplicación independiente**

1. Apuntar el dominio que se va a utilizar a la carpeta public. Es ideal colocar la carpeta mapa_app por fuera del servidor web. Si se coloca varios niveles atras se debe referenciar esta carpeta en el archivo index.php que esta en la public.


**Si se va a utilizar mediante IFRAMES**

1. Embeba la aplicación colocando en la fuente del IFRAME la ruta "http://www.aplicativo.com"  y asegurese de colocarle un tamaño fijo y de eliminar los bordes. el tamaño style="border: none; width: 100%; height: 100%;". Puede ver un ejemplo de esto en  www.aplicativo.com/frame.php
2. Además asegurese que el DIV o el contenedor del frame no tenga margenes, padding y scroll. (style="overflow: hidden; margin: 0; padding: 0;")
