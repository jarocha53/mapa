<html>
	<head>
		<title>Mapa  interactivo</title>
		<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
		<link media="all" type="text/css" rel="stylesheet" href="stylesheets/styles.css">
	</head>
	<body>
		<div id="map-content" class="container-fluid">

		</div>

		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="javascripts/amcharts/ammap/ammap.js" ></script>
		<script type="text/javascript" src="javascripts/amcharts/ammap/maps/js/colombiaLow.js" ></script>
		<script type="text/javascript" src="javascripts/mapa_embed.js" ></script>
	</body>
</html>
