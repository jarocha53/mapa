$(document).ready(function() {

    if ($("#map-content") != undefined) {
        $.getJSON(
            "embed", {},
            function(json) {
                var result = json.html;
                $("#map-content").html(result);
                initialize_map();
            }
        );
    }

});


function initialize_map() {

    $('.link-department').click(function(e) {
        e.preventDefault();
        department_id = $(this).attr("department_id");
        changeDepartment(department_id, false);
    });


    $('.link-city').click(function(e) {
        e.preventDefault();
        $(".city-name").removeClass("active");
        $(this).parent().addClass("active");

        city_id = $(this).attr("city_id");
        changeCity(city_id)
    });

    AmCharts.ready(function() {
        // create AmMap object
        var map = new AmCharts.AmMap();
        // set path to images
        map.pathToImages = "javascripts/amcharts/ammap/images/";

        /* create data provider object
         map property is usually the same as the name of the map file.

         getAreasFromMap indicates that amMap should read all the areas available
         in the map data and treat them as they are included in your data provider.
         in case you don't set it to true, all the areas except listed in data
         provider will be treated as unlisted.
         */
        var dataProvider = {
            map: "colombiaLow",
            getAreasFromMap: true,
        };
        // pass data provider to the map object
        map.dataProvider = dataProvider;

        /* create areas settings
         * autoZoom set to true means that the map will zoom-in when clicked on the area
         * selectedColor indicates color of the clicked area.
         */
        map.areasSettings = {
            autoZoom: false,
            selectable: true,
            selectedColor: "#666",
            color: "#d0d0d0",
            unlistedAreasColor: "#e2e2e2",
            rollOverColor: "#f3fb7f",
            rollOverOutlineColor: "#c00",
        };

        // let's say we want a small map to be displayed, so let's create it
        // map.smallMap = new AmCharts.SmallMap();

        // write the map to container div
        map.write("map-div");

        map.addListener("clickMapObject", function(event) {
            department_id = event.mapObject.id;
            if ($("#content-department-" + department_id) != undefined) {
                changeDepartment(department_id, true);
            }
        })
    });
}

function changeDepartment(department_id, map_scroll) {

    open = true;
    cities = $("#content-department-" + department_id).find(".content-city").filter(":first").addClass("active");
    if ($("#content-department-" + department_id).hasClass("active")) {
        $("#content-department-" + department_id).removeClass("active");
        $("#content-department-" + department_id).parent().find('.department-name').removeClass("active");
        open = false;
    }
    if (map_scroll) {
        $(".department-name").removeClass("active");
        $(".content-department").removeClass("active");
    }
    if (!$("#content-department-" + department_id).hasClass("active") && open) {
        $("#content-department-" + department_id).parent().find('.department-name').addClass("active");
        $("#content-department-" + department_id).addClass("active");
    }

    if (open && map_scroll)
        if (map_scroll) {
            if ($("#content-department-" + department_id).length) {
                $('.m-offices-list').scroll();

                $('.m-offices-list').stop().animate({
                    scrollTop: $('.m-offices-list').scrollTop() + $("#content-department-" + department_id).position().top - 130
                }, 800);
                return false;
            }
        } else {
            $('.m-offices-list').scroll();
            $('.m-offices-list').scrollTop(0);
            $('.m-offices-list').scrollTop($('.m-offices-list').scrollTop() + $("#content-department-" + department_id).position().top - 130);
        }

}

function changeCity(city_id) {
    // $(".content-city").removeClass("active");
    if ($("#content-city-" + city_id).hasClass("active"))
        $("#content-city-" + city_id).removeClass("active");
    else
        $("#content-city-" + city_id).addClass("active");
}