-- phpMyAdmin SQL Dump
-- version 4.0.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2014 at 08:44 PM
-- Server version: 5.5.28
-- PHP Version: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `servicio_empleo_mapa`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `city_code` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cities_city_code_unique` (`city_code`),
  KEY `cities_department_id_foreign` (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` varchar(15) DEFAULT NULL,
  `department_code` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `departments_code_unique` (`code`),
  UNIQUE KEY `departments_department_code_unique` (`department_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`, `code`, `department_code`) VALUES
(34, 'Amazonas', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-AMA', '91'),
(35, 'Antioquia', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-ANT', '05'),
(36, 'Arauca', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-ARA', '81'),
(37, 'Atlántico', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-ATL', '08'),
(38, 'Bolívar', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-BOL', '13'),
(39, 'Boyacá', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-BOY', '15'),
(40, 'Cauca', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-CAU', '19'),
(41, 'Cesar', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-CES', '20'),
(42, 'Chocó', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-CHO', '27'),
(43, 'Caldas', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-CAL', '17'),
(44, 'Córdoba', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-COR', '23'),
(45, 'Caquetá', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-CAQ', '18'),
(46, 'Casanare', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-CAS', '85'),
(47, 'Cundinamarca', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-CUN', '25'),
(48, 'Bogotá, Distrito Capital', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-DC', '11'),
(49, 'Guainía', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-GUA', '94'),
(50, 'Guaviare', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-GUV', '95'),
(51, 'Huila', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-HUI', '41'),
(52, 'La Guajira', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-LAG', '44'),
(53, 'Magdalena', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-MAG', '47'),
(54, 'Meta', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-MET', '50'),
(55, 'Nariño', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-NAR', '52'),
(56, 'Norte de Santander', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-NSA', '54'),
(57, 'Putumayo', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-PUT', '86'),
(58, 'Quindío', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-QUI', '63'),
(59, 'Risaralda', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-RIS', '66'),
(60, 'Santander', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-SAN', '68'),
(61, 'Sucre', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-SUC', '70'),
(62, 'San Andrés, Providencia y Santa Catalina', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-SAP', '88'),
(63, 'Tolima', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-TOL', '73'),
(64, 'Valle del Cauca', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-VAC', '76'),
(65, 'Vichada', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-VID', '99'),
(66, 'Vaupés', '2014-04-07 10:59:09', '2014-04-07 10:59:09', 'CO-VAU', '97');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE IF NOT EXISTS `offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity` varchar(145) NOT NULL,
  `contact_phone` varchar(45) DEFAULT NULL,
  `contact_mobile` varchar(45) DEFAULT NULL,
  `contact_email` varchar(145) DEFAULT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `offices_city_id_foreign` (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offices`
--
ALTER TABLE `offices`
  ADD CONSTRAINT `offices_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE;
