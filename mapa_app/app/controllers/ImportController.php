<?php
class ImportController extends BaseController
{

	public function index()
	{
		$row = 1;
		$total = 0;
		$i = 0;
		if (($handle = fopen("../mapa_app/import_files/mapa.csv", "r")) !== false)
		{
			$cities = City::all();
			foreach ($cities as $city)
			{
				$city->delete();
			}

			while (($data = fgetcsv($handle, 0, ";")) !== false)
			{
				if (sizeof($data) == 9)
				{
					$department_code = $data[0];
					$department = Department::where('department_code', '=', $department_code)->first();
					if (!empty($department) && !empty($department->id))
					{
						$city = null;
						$city_name = $data[2];
						$city_code = $data[3];
						if(empty($city_code))
							$city_code = null;
						else
							$city = City::where('city_code', "=", $city_code)->where('department_id', "=", $department->id)->first();
						if(empty($city))
							$city = City::where('name', "=", $city_name)->where('department_id', "=", $department->id)->first();
						if (empty($city))
						{
							$city = new City();
							$city->name = $city_name;
							$city->department_id = $department->id;
							$city->city_code = $city_code;
							$city->save();
						}

						if (!empty($city))
						{
							$city_code = $data[3];
							$entity = $data[4];
							$address = $data[5];
							$contact_phone = $data[6];
							$contact_mobile = $data[7];
							$contact_email = $data[8];

							$office = new Office;
							$office->city_id = $city->id;
							$office->entity = $entity;
							$office->address = $address;
							$office->contact_phone = $contact_phone;
							$office->contact_mobile = $contact_mobile;
							$office->contact_email = $contact_email;
							try
							{
								$res = $office->save();

								if (!empty($office->id))
								{
									echo "$row. Success - $office->id - fila $row";
									$total++;
								}
								else{
									echo "$i. ERROR - fila $row";
								}
							}
							catch(\Exception $e)
							{
								//Do something when query fails.
								echo "Error linea $row<br/>";
							}
							echo "<br/>";
							$i++;
						}
					}
					else{
						echo "Error linea - Depto not found - fila $row - $department_code<br/>";
					}
				}
				$row++;
			}
			fclose($handle);
		}
		die();
	}
}

