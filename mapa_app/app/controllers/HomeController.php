<?php

class HomeController extends BaseController {

	public function index()
	{
		$departments = Department::all();
		return View::make('mapa', array('departments' => $departments));
	}

	public function embed()
	{
		$departments = Department::all();
		$this->layout = "ajax_layout";

		if (Request::ajax()){
			$html = View::make('mapa', array('departments' => $departments))->render();
            return Response::json(array('html' => $html));
		}
		die();
	}
}


