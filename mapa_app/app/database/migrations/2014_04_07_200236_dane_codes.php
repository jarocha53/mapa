<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DaneCodes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('departments', function(Blueprint $table)
		{
			$table->string('department_code', 15 )->unique()->nullable();
		});

		Schema::table('cities', function(Blueprint $table)
		{
			$table->string('city_code', 15 )->unique()->nullable();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('departments', function(Blueprint $table)
		{
			$table->dropColumn('department_code');
		});

		Schema::table('cities', function(Blueprint $table)
		{
			$table->dropColumn('city_code');
		});
	}

}
