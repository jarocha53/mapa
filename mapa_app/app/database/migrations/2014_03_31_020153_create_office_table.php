<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offices', function($table)
		{
			$table->increments('id');
			$table->string('entity', 145);
			$table->string('address', 145)->nullable();
			$table->string('contact_phone', 45)->nullable();
			$table->string('contact_mobile', 45)->nullable();
			$table->string('contact_email', 145)->nullable();
			$table->integer('city_id')->unsigned();
			$table->foreign('city_id')->references('id')->on('cities');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offices');
	}

}
