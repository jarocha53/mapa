<?php
class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('DepartmentTableSeeder');
		$this->call('CityTableSeeder');
		$this->call('OfficeTableSeeder');
	}
}

class DepartmentTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('departments')->delete();

		Department::create(array('name' => 'Amazonas', 'code' => 'CO-AMA'));

		Department::create(array('name' => 'Antioquia', 'code' => 'CO-ANT'));

		Department::create(array('name' => 'Arauca', 'code' => 'CO-ARA'));

		Department::create(array('name' => 'Atlántico', 'code' => 'CO-ATL'));

		Department::create(array('name' => 'Bolívar', 'code' => 'CO-BOL'));

		Department::create(array('name' => 'Boyacá', 'code' => 'CO-BOY'));

		Department::create(array('name' => 'Cauca', 'code' => 'CO-CAU'));

		Department::create(array('name' => 'Cesar', 'code' => 'CO-CES'));

		Department::create(array('name' => 'Chocó', 'code' => 'CO-CHO'));

		Department::create(array('name' => 'Caldas', 'code' => 'CO-CAL'));

		Department::create(array('name' => 'Córdoba', 'code' => 'CO-COR'));

		Department::create(array('name' => 'Caquetá', 'code' => 'CO-CAQ'));

		Department::create(array('name' => 'Casanare', 'code' => 'CO-CAS'));

		Department::create(array('name' => 'Cundinamarca', 'code' => 'CO-CUN'));

		Department::create(array('name' => 'Distrito Capital de Bogotá', 'code' => 'CO-DC'));

		Department::create(array('name' => 'Guainía', 'code' => 'CO-GUA'));

		Department::create(array('name' => 'Guaviare', 'code' => 'CO-GUV'));

		Department::create(array('name' => 'Huila', 'code' => 'CO-HUI'));

		Department::create(array('name' => 'La Guajira', 'code' => 'CO-LAG'));

		Department::create(array('name' => 'Magdalena', 'code' => 'CO-MAG'));

		Department::create(array('name' => 'Meta', 'code' => 'CO-MET'));

		Department::create(array('name' => 'Nariño', 'code' => 'CO-NAR'));

		Department::create(array('name' => 'Norte de Santander', 'code' => 'CO-NSA'));

		Department::create(array('name' => 'Putumayo', 'code' => 'CO-PUT'));

		Department::create(array('name' => 'Quindío', 'code' => 'CO-QUI'));

		Department::create(array('name' => 'Risaralda', 'code' => 'CO-RIS'));

		Department::create(array('name' => 'Santander', 'code' => 'CO-SAN'));

		Department::create(array('name' => 'Sucre', 'code' => 'CO-SUC'));

		Department::create(array('name' => 'San Andrés, Providencia y Santa Catalina', 'code' => 'CO-SAP'));

		Department::create(array('name' => 'Tolima', 'code' => 'CO-TOL'));

		Department::create(array('name' => 'Valle del Cauca', 'code' => 'CO-VAC'));

		Department::create(array('name' => 'Vichada', 'code' => 'CO-VID'));

		Department::create(array('name' => 'Vaupés', 'code' => 'CO-VAU'));
	}
}

class CityTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('cities')->delete();

		$departments = Department::all();

		foreach ($departments as $department)
		{
			if (rand(0, 10) != 2 && rand(0, 10) != 4)
			{
				$i = 1;
				while ($i <= 4)
				{
					$city = new City;
					$city->name = "Ciudad $i";
					$city->department_id = $department->id;
					$city->save();
					$i++;
				}
			}
		}
	}
}


class OfficeTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('offices')->delete();

		$cities = City::all();

		foreach ($cities as $city)
		{
			if (rand(0, 10) != 2 && rand(0, 10) != 4)
			{
				$i = 1;
				while ($i <= 3)
				{
					$office = new Office;
					$office->entity = "Entidad $i";
					$office->address = "Calle $i".$city->id.$city->department_id;
					$office->contact_email = "info@office_$i$city->id.com";
					$office->contact_phone = rand(7111111, 7222333);
					$office->contact_mobile = rand(3117111111, 3117222333);
					$office->city_id = $city->id;
					$office->save();
					$i++;
				}
			}
		}
	}
}
