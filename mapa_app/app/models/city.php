<?php
/**
 *
 */
class City extends Eloquent {

    public function department()
    {
        return $this->belongsTo('department');
    }

    public function offices()
    {
        return $this->hasMany('office');
    }
}
