<?php
/**
 *
 */
class Department extends Eloquent {

    public function cities()
    {
        return $this->hasMany('city');
    }

}
