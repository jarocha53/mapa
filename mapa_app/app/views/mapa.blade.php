@extends('layout')


@section('content')
<div class="row">
	<div class="m-header col-xs-12 col-md-12 col-lg-12">
		<header>
			<h1 class="app-title">Puntos de servicio</h1>
		</header>
	</div>
</div>

<div class="row">
	<div class="m-map col-xs-0 col-sm-6 col-md-6 col-lg-6 hidden-xs">
		<div id="map-div">

		</div>
	</div>
	<div id="m-offices-list" class="m-offices-list col-xs-12 col-sm-5 col-md-5 col-lg-5 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
		<?php $i=0; ?>
		@foreach($departments as $department)
		<div class="m-department">
			<section>
				<h1 class="department-name <?php if($i == 0) echo "active"; ?>">
					<a rel="nofollow" class="link-department" department_id="{{ $department->code }}">{{ $department->name }}</a>
				</h1>
				<div class="content-department <?php if($i == 0) echo "active"; ?>" id="content-department-{{ $department->code }}">
					<?php $j=0; ?>
					@if (count($department->cities) > 0)
					@foreach($department->cities as $city)
					<div class="m-city ">
						<h2 class="city-name">
							<a rel="nofollow" class="link-city" city_id="{{ $city->id }}">{{ $city->name }}</a>
						</h2>
						<div class="content-city <?php if($j == 0) echo "active"; ?>" id="content-city-{{ $city->id }}">
							@if (count($city->offices) > 0)
							@foreach($city->offices as $office)

							<div class="m-office">
								<h3 class="office-name">{{{ $office->entity  }}}</h3>
								<p class="address">{{{ $office->address  }}}</p>
								<p class="contact-info">{{{ $office->contact_phone or '' }}}</p>
								<p class="contact-info">{{{ $office->contact_email or '' }}}</p>
							</div>
							@endforeach
							@else
							<p>
								Lo sentimos, no hay puntos de servicio en {{{$city->name}}}
							</p>
							@endif

						</div>
					</div>
					<?php $j++; ?>
					@endforeach
					@else
					<p>
						Lo sentimos, no hay puntos de servicio en {{{$department->name}}}
					</p>
					@endif
				</div>
			</section>
		</div>
		<?php $i++; ?>
		@endforeach
	</div>
</div>
@stop