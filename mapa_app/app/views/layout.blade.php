@if(!Request::ajax())
<html>
<head>
	<title>Mapa  interactivo</title>

	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
	{{ HTML::style('stylesheets/styles.css') }}

</head>
<body data-grid-framework="b3f" data-grid-color="red" data-grid-opacity="0.05" data-grid-zindex="10" data-grid-gutterwidth="20px" data-grid-nbcols="12">
	<div class="container-fluid">
		@endif

		@yield('content')

		@if(!Request::ajax())
	</div>

	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	{{ HTML::script('javascripts/amcharts/ammap/ammap.js') }}
	{{ HTML::script('javascripts/amcharts/ammap/maps/js/colombiaLow.js') }}
	{{ HTML::script('javascripts/mapa.js') }}

</body>
</html>
@endif